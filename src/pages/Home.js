import React, { useContext, useEffect, useState } from 'react';
import { signOut } from "firebase/auth";
import { useNavigate } from 'react-router-dom';
import UserDataForm from "../components/userDataForm";
import { UserContext } from "../context/userContext";
import { auth, db } from '../firebase';
import { onAuthStateChanged } from "firebase/auth";
// import { uid } from 'uid';
import { set, ref, onValue } from "firebase/database";
import { RevealJS } from 'reveal.js';
import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Slide from 'react-reveal/Slide';

// import 'dist/theme/black.css';

export default function Home() {
    const navigate = useNavigate();
    const [ datasUser, setDatasUser] = useState([]);

    useEffect(() => {
        auth.onAuthStateChanged((user) => {
            if(user) {
                onValue(ref(db, `users/${auth.currentUser.uid}`), snapshot => {
                    setDatasUser([]);
                    const data = snapshot.val();
                    if(data !== null) {
                        Object.values(data).map(datasUser => {
                            setDatasUser((oldArray) => [...oldArray, datasUser]);

                            console.log(datasUser);
                        })
                    }
                })
            }
            else if(!user) {
                navigate(("/login"));
            }
        });
    }, []);

    console.log(datasUser);

    const logOut = async () => {
        try {
            await signOut(auth);
            navigate("/login")
        } catch {
            alert("Pour certaines raisons nous ne pouvons pas vous déconnecter, veuillez vérifier votre connexion et réessayer");
        }
    }

    if (datasUser[0]) {
        return (
            <div>
                <h1>HOME PAGE</h1>
                <button onClick={logOut} className='logout-button'>Se déconnecter</button>
            </div>
        );
    }
    else {
        return (
            <div>
                < UserDataForm/>
            </div>
        );
    }   
};
