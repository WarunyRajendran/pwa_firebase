import React, { useContext, useState, useRef, useEffect } from 'react';
import { signOut } from "firebase/auth";
import { auth, db } from '../firebase';
import { UserContext } from "../context/userContext";
import forgotPassword from '../pages/forgotPassword';
import '../styles/login.css';
import { useNavigate } from "react-router-dom";
import {   
  onAuthStateChanged,
  GoogleAuthProvider,
  signInWithPopup
} from "firebase/auth";
import {
  Link
} from "react-router-dom";

export default function Login() {

  const { signIn } = useContext(UserContext);
  const navigate = useNavigate();

  const [validation, setValidation] = useState("");
  const provider = new GoogleAuthProvider();

  provider.setCustomParameters({
    prompt: "select_account"
  });

  // Ref permet de faire des références, sélection des éléments avec React
  const inputs = useRef([]);
  const addInputs = el => {
    if(el && !inputs.current.includes(el)) {
      inputs.current.push(el); // va rajouter tous les éléments dans le tableau inputs
    }
  }

  const formRef = useRef();

  const handleForm = async (e) => {
    e.preventDefault();

    try {
      // retourne l'objet crée
      const cred = await signIn(
        inputs.current[0].value,
        inputs.current[1].value,
      )
      setValidation("");
      navigate("/");

    } catch {
        setValidation("Identifiant ou mot de passe incorrect");
    }
  }


  const signInWithGoogle = () => {
    signInWithPopup(auth, provider)
    .then((result) => {
      console.log(result);
      // const name = result.user.displayName
      // const email = result.user.email
      // const profilPic = result.user.photoURL;

      // localStorage.setItem("name", name)
      // localStorage.setItem("email", email)
      // localStorage.setItem("profilPic", profilPic)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  useEffect(() => {
    onAuthStateChanged(auth, (response) => {
      if(response) {
        navigate("/");
      }
      else {
        navigate("/login");
      }
    })
  }, [])

  return (
    <>
      <h1 className='title-connexion'>Connexion</h1>
      <div className='form-box-login'>
        <p className='validation-text'>{validation}</p>
        <form ref={formRef} onSubmit={handleForm}>
          <div>
            <label htmlFor='loginEmail'>Email </label>
            <input ref={addInputs} type='email' name='email' id='loginEmail' required />
          </div>
          <div>
            <label htmlFor='loginPassword'>Mot de passe </label>
            <input ref={addInputs} type='password' name='pwd' id='loginPassword' required />
          </div>

          <button className="connect-button">Se connecter</button>
        </form>
        <button type="button" className="login-with-google-btn" onClick={signInWithGoogle}>Se connecter avec Google</button>
        <Link to='/forgot-password' className='link-forgot-pwd'>Mot de passe oublié ?</Link>
      </div>
      <div className='container-link-login'>
        <span>Pas encore inscrit ?</span>
        <Link to='/register' className='link-login'>Inscription</Link>
      </div>
    </> 
  )
};

