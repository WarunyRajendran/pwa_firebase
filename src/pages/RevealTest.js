import React from 'react';
import { RevealJS } from 'reveal.js';
import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Slide from 'react-reveal/Slide';

const RevealTest = () => {
    return (
        <div>
            <h1>Reveal Base test</h1>

            <div>
                <Slide left>
                    <section>
                        <h1>React Reveal</h1>
                    </section>
                </Slide>
            </div>
            
            
        </div>
    );
};

export default RevealTest;