import firebase from 'firebase/compat/app';
import { initializeApp } from 'firebase/app';
import 'firebase/compat/database';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';

var firebaseConfig = {
  apiKey: "AIzaSyBJmvbd-Qz-bRtqXavdwE3mWDqRxPhC7fw",
  authDomain: "pwaslide.firebaseapp.com",
  databaseURL: "https://pwaslide-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "pwaslide",
  storageBucket: "pwaslide.appspot.com",
  messagingSenderId: "446573800741",
  appId: "1:446573800741:web:222adb453513057232fecc",
  measurementId: "G-VHRFK14VD6"
}

const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);
export const auth = getAuth();