import { useState, useEffect } from "react";
import { auth, db } from "../firebase";
import '../styles/userDataForm.css';
import { set, ref } from "firebase/database"

export default function UserDataForm() {
    const [lastname, setLastname] = useState("");
    const [firstname, setFirstname] = useState("");
    const [isShowing, setIsShowing] = useState(false);

    const writeToDatabase = () => {
        set(ref(db, `users/${auth.currentUser.uid}`), {
            lastname: lastname,
            firstname: firstname,
        });
        setLastname("");
        setFirstname("");
    };
    

    return (
        <>
            <h1 className="title-user-data">Informations utilisateurs</h1>
            <div className="form-box-data">
                <div className="contenu">
                    <div>
                        <label>Nom </label>
                        <input type="text" placeholder="Nom" value={lastname} 
                        onChange={(e) => setLastname(e.target.value)} />
                    </div>

                    <div>
                        <label>Prénom </label>
                        <input type="text" placeholder="Prénom" name="firstname" value={firstname} 
                        onChange={(e) => setFirstname(e.target.value)} />
                    </div>

                    <button onClick={writeToDatabase} className="save-button-data">Enregistrer</button>
                </div>
            </div>
        </>
    );
};