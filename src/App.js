import logo from './logo.svg';
import firebase from 'firebase/compat/app';
import React, { useEffect, useState } from 'react';
import { dataRef } from './firebase';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Link
} from "react-router-dom";
import Register from "./pages/Register";
import Login from "./pages/Login";
import ForgotPassword from "./pages/forgotPassword";
import Home from "./pages/Home"
import Reveal from "./pages/RevealTest"
import { keyboard } from '@testing-library/user-event/dist/keyboard';
import '/node_modules/reveal.js/dist/reveal.css';
import '/node_modules/reveal.js/dist/theme/black.css';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/reveal" element={<Reveal />} />
        <Route exact path="/forgot-password" element={<ForgotPassword />} />
        <Route exact path="/register" element={<Register />} />
        <Route exact path="/login" element={<Login />} />
      </Routes>
    </Router>
  );
};


export default App;
